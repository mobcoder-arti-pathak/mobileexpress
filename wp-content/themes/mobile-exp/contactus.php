   <?php /* Template Name: contactus */ get_header();?>
   <?php
   if(!empty($_POST)){
    echo '<script>alert("Thanks For Visiting Mobile App Express");</script>'; 
      $userEmail = $_POST['userEmail'];
      $to=$userEmail;
      $subject='Contact Us Form';
      $body='Name :'.$_POST['fullName'] . '<br> Company Name : '.$_POST['companyName']. '  <br>  Mobile : '.$_POST['mobile']. ' <br> Budget = '.$_POST['budget']. '<br> Project Description :'.$_POST['projectDescription'];
      //select admin email id of member
    $header = "From: contact@MobileAppExpress.com\r\n"; 
	$header.= "MIME-Version: 1.0\r\n"; 
	$header.= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
	$header.= "X-Priority: 1\r\n";
      if(@mail($to,$subject,$body,$header)){
        echo '<script>alert("Thanks For Visiting Mobile App Express");</script>'; 
   }else{
	 
   }
   }
   ?>
   <div id="banner" class="height banner-h">
   <div class="cover" style="background-image: url(<?php echo get_theme_mod("custom_contactus_banner_imageControl_setting");?>)"></div>
    <div class="banner-in">
      <div class="banner-info text-center">
        <div class="container">
          <h2 class="banner-title wow fadeInUp" data-wow-duration="1s"><?php echo get_theme_mod("custom_contactus_title_setting");?></h2>
          <p class="line wow fadeInUp" data-wow-duration="2s"><?php echo get_theme_mod("custom_contactus_subtitle_setting");?><br>
           </p>
        </div>
      </div>
    </div>
  </div>
  <section id="content">
    <div class="section gray-bg contactus-sec">
      <div class="container-fluid">
        <div class="content-head text-center wow fadeInUp" data-wow-duration="1s">
          <h1 class="title">TYPE OF PROJECT</h1>
        </div>
        <!--content-head-->

        <div class="row box-group" data-toggle="buttons">
			<div class="col-md-3 col-sm-6 col-xs-12">
			<label class="btn">
			   <div class="box text-center type-project"> 
                      <span class="icon">
                        <img src="<?php bloginfo('template_url');?>/assets/images/ios-icon.png" alt=""></span>
                      <p class="color">iOS APP </p>
                  </div>
			</label>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
			<label class="btn">
				<div class="box text-center type-project"> 
                <span class="icon">
                    <img src="<?php bloginfo('template_url');?>/assets/images/android-icon.png" alt=""></span>
                    <p class="color">ANDROID APP </p>
            </div>
			</label>			
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
			<label class="btn ">
				<div class="box text-center type-project"> 
                <span class="icon">
                    <img src="<?php bloginfo('template_url');?>/assets/images/windows.png" alt=""></span>
                    <p class="color">WEB APP </p>
            </div>
			</label>			
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
			<label class="btn ">
				<div class="box text-center type-project"> 
                <span class="icon">
                    <img src="<?php bloginfo('template_url');?>/assets/images/ui-ux-icon.png" alt=""></span>
                    <p class="color">UI/UX </p>
            </div>
			</label>			
            </div>
					
		
		</div>
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
<!--
        <div class="row">
          <div class="col-md-3">
            <div class="box text-center type-project"> 
                <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/ios-icon.png" alt=""></span>
              <p class="color">iOS APP </p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box text-center type-project"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/android-icon.png" alt=""></span>
              <p class="color">ANDROID APP </p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box text-center type-project"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/windows.png" alt=""></span>
              <p class="color">WEB APP </p>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box text-center type-project"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/ui-ux-icon.png" alt=""></span>
              <p class="color">UI/UX</p>
            </div>
          </div>
        </div>
-->
      </div>
    </div>
    <!--section-->
    
    <div class="section">
      <div class="container">          
<?php echo do_shortcode('[contact-form-7 id="303" title="Contact Us page form"]'); ?>

<!--        <form class="contact-form" method='POST' action=''>
          <div class="row">
            <div class="col-md-6 ">
            <h1 class="title1 text-center color">CONTACT INFO</h1>

              <div class="form-group">
                <label>FULL NAME *</label>
                <input type="text" name="fullName" id="fullname" class="form-control">
                <label id="nameLabel"></label>
              </div>
              <div class="form-group">
                <label>COMPANY NAME *</label>
                <input type="text" name="companyName" id="companyname" class="form-control">
                <label id="companyLabel"></label>
              </div>
              <div class="form-group">
                <label>EMAIL ADDRESS *</label>
                <input type="text" name="userEmail" id="usermail" class="form-control">
                <label id="emailLabel"></label>
              </div>
              <div class="form-group">
                <label>PHONE NUMBER *</label>
                <input type="text" name="mobile" id="mobile" class="form-control">
                <label id="telephoneLabel"></label>
              </div>
              <div class="form-group">
                <label>BUDGET</label>
				<select class="budget-select-list">
						<option value="1">200$-250$</option>
						<option value="2">300$-350$</option>
						<option value="3">400$-450$</option>
						<option value="3">450$-600$</option>
						<option value="3">1000$-1500$</option>
						<option value="3" selected="">Above 2000$</option>
					</select>
               </div>
                 <div class="form-group">
                <input type="hidden" name="project" id="project" class="form-control">
                <label id="telephoneLabel"></label>
              </div>
            </div>
            <div class="col-md-6">
               <h1 class="title1 text-center color">PROJECT INFO</h1>

              <div class="form-group">
                <label>PROJECT DESCRIPTION</label>
                <textarea class="form-control" id="messageInput" name="projectDescription"></textarea>
                <label id="messageLabel"></label> 
              </div>
            </div>
          </div>
          
          <div class="btn-row text-center">
          	 <button type = "submit" class="custom-btn submit">Let’s Talk</button>
          </div>
         </form>-->
    </div>
     </div>
    
    <h1 class="title info-h text-center">MORE CONTACT INFO</h1>
    <div class="contact-info">
    <div class="cover" style="background-image: url(<?php echo get_theme_mod("custom_contactus_moreinfo_img_setting");?>)"></div>
     <div class="container">
     <div class="row">
     	  	<div class="col-md-6 col-12">
     		<div class="info-col">
     			<span class="info-t">EMAILS</span><span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
     			<span class="email-id">
     			<a href="mailto:Contact@MobileAppExpress.com"><?php echo get_theme_mod("custom_contactus_email2_setting");?></a>
     			<a href="mailto:Careers@MobileAppExpress.com"><?php echo get_theme_mod("custom_contactus_email_setting");?></a>
     			</span>
     		</div>
     	</div>
     	
     		<div class="col-md-6 col-12">
     		<div class="info-col">
     			<span class="info-t">PHONE NUMBERS</span><span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
     			<p class="email-id">
     			 <span><small>TOLL-FREE</small><a href="tel:800-610-4054"><?php echo get_theme_mod("custom_contactus_phonenum_setting");?></a></span>
     			 <span><small>TOLL-FREE</small><a href="tel:800-610-4054"><?php echo get_theme_mod("custom_contactus_phonenum2_setting");?></a></span>
      			</p>
     		</div>
     	</div>
     </div>
       </div>
      <div class="clearfix"></div>
    </div>
   </section>
     <div class="section gray-bg text-center privacy">
  	<div class="container">
  		<h1 class="title info-h">PRIVACY</h1>
  		<p class="color2"><?php echo get_theme_mod("custom_contactus_privacy_setting");?></p>
  	</div>
  </div>
  <!--content-->
  <?php get_footer();?>
