   <?php /* Template Name: process */ get_header();?>
   <div id="banner" class="height">
    <div class="banner-in">
      <div class="banner-info">
        <div class="container">
          <div class="banner-content text-left wow fadeInUp" data-wow-duration="1s">
            <h2 class="banner-title"><?php echo get_theme_mod("custom_processsbanner_title_setting",'Process');?></h2>
            <p><?php echo get_theme_mod("custom_processs_banner_description_setting");?></p>
            <a href="contactus" class="custom-btn">Let’s Talk</a> </div>
          <div class="image text-right wow fadeInUp progress-img" data-wow-duration="1s">
		 <img src="<?php echo get_theme_mod("custom_process_imageControl_setting");?>">
		  </div>
        </div>
      </div>
    </div>
  </div>
  <section id="content">
    <div id="what-we" class="section process">
      <div class="container-fluid">
        <div class="content-head text-center wow fadeInUp" data-wow-duration="1s">
          <h1 class="title">3-STEP PROCESS</h1>
         </div>
        <!--content-head-->
     <div class="process-content">
     	<div class="row">
     		<div class="col-md-4">
     			<div class="process-box text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1s">
     			<img src="<?php bloginfo('template_url');?>/assets/images/research.svg" alt="">
     			<h2 class="sub-title">Initiation <br>& Research</h2>
     			</div>
     		</div>
     		<div class="col-md-4">
     			<div class="process-box text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1.1s" >
     			<img src="<?php bloginfo('template_url');?>/assets/images/prototyping.svg" alt="">
     			    			<h2 class="sub-title">UI/UX <br>Prototyping</h2>
     			</div>
     		</div>
     		<div class="col-md-4">
     			<div class="process-box text-center wow zoomIn" data-wow-duration="1s" data-wow-delay="1.2s" >
     			<img src="<?php bloginfo('template_url');?>/assets/images/delivery.svg" alt="">
     			<h2 class="sub-title">Execution <br>& Delivery</h2>
     			</div>
     		</div>
      	</div>
      </div>
         
      </div>
    </div>
    <!--what-we-->
    
    
    
    <div class="step-setion">
 	  <div class="step-col-left">
		<div class="step-content">
		 <div class="step-head">
		 <span class="img">
	  <img src=" <?php echo get_theme_mod("process_step1_iconControl_setting");?>">

		 	
		 </span>	<small>STEP 1</small>
		 	<h2 class="sub-title"><?php echo get_theme_mod("custom_process_step1_title_setting");?></h2>
		 </div><!--step-head-->
 		 <p><?php echo get_theme_mod("custom_process_step1_desc_setting");?></p>
 		  </div>	
  		</div>
	  		<div class="step-col-right"><div class="image-step">
				<div class="cover" style="background-image:url(<?php echo get_theme_mod("process_step1_imageControl_setting");?>)"> </div>
				</div></div>
 	  </div>
 	  
 	  
 	   <div class="step-setion">
 	  <div class="step-col-left">
		<div class="step-content">
		 <div class="step-head">
		 <span class="img">
		 	
		 		  <img src=" <?php echo get_theme_mod("process_step2_iconControl_setting");?>">

		 </span>	
		 <small>STEP 2</small>
		 	<h2 class="sub-title"><?php echo get_theme_mod("custom_process_step2_title_setting");?></h2>
		 </div><!--step-head-->
 		 <p><?php echo get_theme_mod("custom_process_step2_desc_setting");?></p>
 		  </div>	
  		</div>
	  		<div class="step-col-right"><div class="image-step">
				<div class="cover" style="background-image:url(<?php echo get_theme_mod("process_step2_imageControl_setting");?>)"> </div>
				</div></div>
 	  </div>
 	  
 	  
 	   <div class="step-setion">
 	  <div class="step-col-left">
		<div class="step-content">
		 <div class="step-head">
		 <span class="img">
		  <img src=" <?php echo get_theme_mod("process_step3_iconControl_setting");?>">
            </span><small>STEP 3</small>
		 	<h2 class="sub-title"><?php echo get_theme_mod("custom_process_step3_title_setting");?></h2>
		 </div><!--step-head-->
 		 <p><?php echo get_theme_mod("custom_process_step3_desc_setting");?></p>
 		  </div>	
  		</div>
	  		<div class="step-col-right">
          <div class="image-step">
			    <div class="cover" style="background-image:url(<?php echo get_theme_mod("process_step3_imageControl_setting");?>)"> </div>
        </div>
      </div>
 	  </div>
    
    
 
    
  </section>
  <!--content-->
  <?php get_footer();?>