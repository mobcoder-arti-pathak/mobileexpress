  <?php get_header();?>
   <div id="content">
    <div class="container">
      <?php 
		while(have_posts()):  the_post();
		get_template_part('template-parts\page\content','page');
		endwhile;
 		?>
     </div>
  </div>
  <!--content-->
 <?php get_footer();?>