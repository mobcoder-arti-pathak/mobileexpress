  <div class="blog-list clearfix post<?php the_ID(); ?>" id="post-<?php the_ID(); ?>">
   <div class="blog-image">
         <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail('post-image');?>
         </a>
        </div>
   <div class="entry-content">
          <h3 class="blog-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
          <div class="info"> <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i><?php the_time('F j, Y') ?></span> <span class="auther"><i class="fa fa-user-o"></i><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><?php the_author(); ?></a></span>
           <!--auther-->
          </div>
  <?php the_excerpt();?>
         </div>
   </div>
 