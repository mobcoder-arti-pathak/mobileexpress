<?php if ( has_post_thumbnail() ) { ?>
<span class="inner-image"><?php the_post_thumbnail(); ?></span>
<?php }?>
 <?php the_content();
 