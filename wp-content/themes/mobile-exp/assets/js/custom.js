$(function () {
 		var filterList = {
 			init: function () {
 				// MixItUp plugin
				// http://mixitup.io
				$('#portfoliolist').mixItUp({
  				selectors: {
    			  target: '.portfolio',
    			  filter: '.filter'	
    		  },
    		  load: {
      		  filter: '.mobile-cat'  
      		}     
				});								
			
			}

		};
		
		// Run the show!
		filterList.init();
 		
	});	
//
//$('.carousel').carousel({
//  interval: 2000
//})


$(document).ready(function() {
	$(window).scroll(function() {
		var sticky = $('#header'),
			scroll = $(window).scrollTop();
		if (scroll > 0){
			sticky.addClass('fixed');
		}else {sticky.removeClass('fixed');} 
		
	});
});
 
$('.arrow a').on('click', function(event) {
     var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
});

 
 new WOW().init();

 
 $(document).ready(function(){
 //$('.submit').click(function(event){
    //validateForm();  
   // event.preventDefault(); 
//});

  $(".contact-form").validate({
    // Specify validation rules
    rules: {
      
      fullName: "required",
      companyName: "required",
	  mobile:{
		  required:true,
		  number:true
	  },
      userEmail: {
        required: true,
        email: true
      },
      
    },
    // Specify validation error messages
    messages: {
      fullName: "Please enter your full name",
      companyName: "Please enter your comapany Name",
	  userEmail: "Please enter a mail",
	  mobile: "Please enter valid mobile number",
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      userEmail: "Please enter  valid email address"
    },
    
    submitHandler: function(form) {
      form.submit();
    }
  });
   
$('.selectpicker').selectpicker();

});


$(document).ready(function(){
    
    $('.type-project').click(function(){
        $('#project').val($('#project').val()+" "+$(this).find('.color').text());
        console.log($(this).find('.color').text());
    });
});

 
            $(document).ready(function() {
              var owl = $('.owl-carousel');
              owl.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1
                  },
                  1000: {
                    items: 1
                  }
                }
              })
            });



$(document).ready(function(){
  $("#demo .carousel-inner .carousel-item:first-child").addClass("active");
 });
 