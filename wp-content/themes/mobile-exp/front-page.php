  <?php get_header();?>  
  <div id="banner">

        <div class="cover" style="background-image: url(<?php echo get_theme_mod("custom_banner_imageControl_setting");?>)">
		
		</div>

    <div class="banner-in text-center">
      <div class="banner-info">
        <div class="container">
          <h2 class="banner-title wow fadeInUp" data-wow-duration="1s"><?php echo get_theme_mod("custom_banner_title_setting");?></h2>
          <p class="wow fadeInUp"  data-wow-duration="1s" data-wow-delay="1s"><?php echo get_theme_mod("custom_theme_banner_description_setting");?></p>
          <a href="#content" class="custom-btn wow fadeInUp" data-wow-duration="2s" data-wow-delay="2s">
		  Learn More</a>
              </div>
      </div>
    </div>
      
  </div>
  <section id="content">
    <div id="what-we" class="section gray-bg">
      <div class="container-fluid">
        <div class="content-head text-center wow fadeInUp" data-wow-duration="1s">
          <h1 class="title">WHAT WE DO</h1>
          <p class="sub-title">We build mobile and web solutions that people use and love. </p>
        </div>
        <!--content-head-->
        
        <div class="row">
        <?php while (have_posts()) : the_post(); ?>
        <div class="col-md-4 col-sm-12 col-xs-12 wow fadeInLeft" data-wow-duration="1s">
            <div class="box text-center"> 
              <span class="icon"><?php the_post_thumbnail();?></span>
              <h2 class="title2 spacing"><?php the_title(); ?></h2>
              <div class="para-color m-0"><?php the_content(); ?></div>
			 
            </div>
          </div>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
    <!--what-we-->
    
    <div id="custom-process" class="section">
      <div class="cover" style="background-image:url(<?php echo get_theme_mod("custom_theme_imageControl_setting");?>);"></div>
      <div class="container-fluid">
        <div class="row">
			<div class="col-md-6 wow fadeInLeft" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInLeft;">
				<div class="image">
			        <img src=" <?php echo get_theme_mod("custom_theme_Banner_imageControl_setting");?>">
			   </div>
          </div>
          <div class="col-md-6 wow fadeInRight" data-wow-duration="1s">
            <div class="custom-process-cnt">
			  <h1 class="title"><?php echo get_theme_mod("custom_theme_title_setting");?></h1>
              <p><?php echo get_theme_mod("custom_theme_description_setting");?></p>
              <div class="text-center m-5"><a href="process" class="custom-btn btn-gray">Learn More</a></div>
            </div>
            <!--custom-process-cnt--> 
          </div>
        </div>
      </div>
    </div>
    <!--custom-process-->
    
    <div id="our-work" class="dark-gray-bg section">
      <div class="our-work-cnt text-center wow fadeInLeft" data-wow-duration="1s">
        <h1 class="title"><?php echo get_theme_mod("custom_our_work_title_setting");?></h1>
        <h2 class="title2"><?php echo get_theme_mod("custom_our_work_subtitle_setting");?></h2>
        <a href="project" class="custom-btn">See Our Work</a> </div>
      <!--our-work-cnt-->
      <div class="mobile wow fadeInUp" data-wow-duration="1s">
	  <img src=" <?php echo get_theme_mod("custom_our_work_imageControl_setting");?>"></div>
    </div>
    <!--what-we-->
      <div id="testimonial" class="text-center">
      <div class="container">
        <h1 class="title wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">CLIENTS</h1>
        <h2 class="title2 wow fadeInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: fadeInUp;">Read what our clients have to say</h2>
			<?php 
			$args = array( 'post_type' => 'testimonials', 'posts_per_page' => 10 );
			$the_query = new WP_Query( $args ); 
			?>
							<div id="demo" class="carousel slide wow fadeInUp" data-wow-duration="1s" data-ride="carousel"> 
					   <div class="carousel-inner">
			  <?php if ( $the_query->have_posts() ) : ?>
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
				  <div class="carousel-item">
						  <div class="test-slide">
					  <?php the_content(); ?>  </div>
						</div>
			  <?php endwhile; ?>
			<?php endif; ?>
           
           </div>
          
           <!-- Left and right controls --> 
          <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> </div>
        
      </div>
    </div>
      
   <!--
    
    <!--testimonial--> 
    
  </section>
  <!--content-->
   <?php get_footer();?>