   <?php /* Template Name: projects */ get_header();?>
   <div id="banner" class="height">
    <div class="banner-in">
      <div class="banner-info">
        <div class="container">
          <div class="banner-content text-left wow fadeInUp" data-wow-duration="1s">
            <h2 class="banner-title">
			<?php echo get_theme_mod("custom_projectsbanner_title_setting");?></h2>
            <p><?php echo get_theme_mod("custom_projects_banner_description_setting");?></p>
            <a href="contactus" class="custom-btn">Let’s Talk</a>
			</div>
            <div class="image text-right wow fadeInUp project-img" data-wow-duration="1s">
		       <img src="<?php echo get_theme_mod("custom_projects_imageControl_setting");?>">
			</div>
        </div>
      </div>
    </div>
  </div>
  <section id="content">
  
     
     <div class="project-section gamemaster">
     	<div class="container">
     	<div class="project-left project-col wow fadeInLeft">
     		<div class="mobile-screen">  
				<img src="<?php echo get_theme_mod("custom_game_masters_imageControl_setting");?>">
           </div>
      	</div>
     	<div class="project-right project-col wow fadeInRight">
     		<div class="project-cnt">
     			<div class="logo"><img src="<?php bloginfo('template_url');?>/assets/images/gamemaster.svg" alt=""></div>
     			<h4 class="sub-title">
				<?php echo get_theme_mod("custom_projects_game_masters_title_setting");?></h4>
     			<p><?php echo get_theme_mod("custom_projects_game_masters_sub_title_setting");?></p>
     			<div class="get-app"><span>Get App</span>
      		<ul>
     				<li>
					 <img src="<?php echo get_theme_mod("custom_game_masters_icon_imageControl_setting");?>">
					</li>
     				<li>
					<img src="<?php echo get_theme_mod("custom_game_masters_icon2_imageControl_setting");?>">
                    </li>
     			</ul>
      			</div><!--get-app-->
     		</div><!--project-cnt-->
       	</div>
 		 </div>
 		 <div class="clearfix"></div>
     </div><!--project-section-->
         <div class="project-section socially-fit">
     	<div class="container">
     	<div class="project-left project-col wow fadeInLeft">
     		<div class="mobile-screen">
			<img src="<?php echo get_theme_mod("custom_f_imageControl_setting");?>"></div>
      	</div>
     	<div class="project-right project-col wow fadeInRight">
     		<div class="project-cnt">
     			<div class="logo"><img src="<?php bloginfo('template_url');?>/assets/images/socially-fit.svg" alt=""></div>
     			<h4 class="sub-title"><?php echo get_theme_mod("custom_projects_F_title_setting");?></h4>
     			<p><?php echo get_theme_mod("custom_projects_F_desc_setting");?></p>
     			<div class="get-app">
     			<span>Get App</span>
				<ul>
     				<li>
					   <img src="<?php echo get_theme_mod("custom_f_icon2_imageControl_setting");?>">
					</li>
     				<li>
					   <img src="<?php echo get_theme_mod("custom_f_icon_imageControl_setting");?>">
					</li>
     		</ul>	
    		</div><!--get-app-->
     	</div><!--project-cnt-->
    </div>
 		 </div>
 		 <div class="clearfix"></div>
    </div><!--project-section-->
     
     
     
       <div id="testimonial" class="text-center">
      <div class="container">
        <h1 class="title wow fadeInUp" data-wow-duration="1s">CLIENTS</h1>
        <h2 class="title2 wow fadeInUp" data-wow-duration="1s">Read what our clients have to say</h2>
        <div id="demo" class="carousel slide wow fadeInUp" data-wow-duration="1s" data-ride="carousel"> 
           <!-- The slideshow -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="test-slide">“The process was easy and simple, I am glad that we chose them. Very easy to talk to.” </div>
            </div>
            <div class="carousel-item">
              <div class="test-slide">“The process was easy and simple, I am glad that we chose them. Very easy to talk to.” </div>
            </div>
            <div class="carousel-item">
              <div class="test-slide">“The process was easy and simple, I am glad that we chose them. Very easy to talk to.” </div>
            </div>
          </div>
          
          <!-- Left and right controls --> 
          <a class="carousel-control-prev" href="#demo" data-slide="prev"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-control-next" href="#demo" data-slide="next"> <span class="carousel-control-next-icon"></span> </a> </div>
      </div>
    </div>
    <!--testimonial--> 
     
     
 
    
  </section>
  <!--content-->
  <!--content-->
  <?php get_footer();?>