   <?php /* Template Name: aboutus */ get_header();?>
   <div id="banner" class="height">
    <div class="banner-in">
      <div class="banner-info">
        <div class="container">
          <div class="banner-content text-left wow fadeInUp" data-wow-duration="1s">
            <h2 class="banner-title"><?php echo get_theme_mod("custom_aboutbanner_title_setting");?></h2>
            <p><?php echo get_theme_mod("custom_about_banner_description_setting");?></p>
            <a href="contactus" class="custom-btn">Let’s Talk</a> 
			</div>
          <div class="image text-right about-img  wow fadeInUp" >
              <div class="img">
			  		  <img src="<?php echo get_theme_mod("custom_about_imageControl_setting");?>" class="">
			  </div>
         </div>
        </div>
      </div>
    </div>
  </div>
  <section id="content">
    <div class="section gray-bg text-center express-section">
      <div class="container">
        <div class="content-head">
          <h1 class="title wow fadeInUp"><?php echo get_theme_mod("mobile_express_title_setting");?></h1>
          <h4 class="sub-title line wow fadeInUp"><?php echo get_theme_mod("mobile_express_subtitle_setting");?></h4>
        </div>
        <p class="wow fadeInUp"><?php echo get_theme_mod("mobile_express_description_setting");?></p>
      </div>
    </div>
    
      <div class="section innovative-section">
      <div class="container">
          <div class="row">
         	<div class="col-md-3 wow fadeInLeft">
			<div class="image">
			    <img src="<?php echo get_theme_mod("innovative_imageControl_setting");?>">
			 </div>
			 </div>
         	<div class="col-md-9 wow fadeInRight">
         	<div class="innovative">
         		<h1 class="title text-center"><?php echo get_theme_mod("innovative_title_setting");?></h1>
         		<p><?php echo get_theme_mod("innovative_description_setting");?></p>
         	</div> <!--innovative--> 
         	 </div>
         	
         </div>
       </div>
    </div>
    
             <div class="section experienced-section">
             <div class="cover" style="background-image:url(<?php echo get_theme_mod("experinced_banner_setting");?>)"></div>
           <div class="container innovative">
          
          <div class="row">
         	<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInLeft"><div class="image">
                <img src="<?php echo get_theme_mod("experinced_image_setting");?>">
                </div></div>
         	<div class="col-md-9 col-sm-6 col-xs-12 wow fadeInRight">
                	<h1 class="title sec-col-title"><?php echo get_theme_mod("experinced_title_setting");?></h1>
         	<div class="innovative">
         	    <p><?php echo get_theme_mod("experinced_description_setting");?></p>
         	</div> <!--innovative--> 
         	 </div>
          </div>
          <div class="technology wow fadeInUp">
          	<div class="technology-col">
			  <img src=" <?php echo get_theme_mod("icon1_setting");?>">
			</div>
          	<div class="technology-col">
			   <img src=" <?php echo get_theme_mod("icon2_setting");?>">
			</div>
          	<div class="technology-col">
			   <img src=" <?php echo get_theme_mod("icon3_setting");?>">
			</div>
          	<div class="technology-col">
			<img src=" <?php echo get_theme_mod("icon4_setting");?>">
			</div>
          	<div class="technology-col">
			<img src=" <?php echo get_theme_mod("icon5_setting");?>">
			</div>
          	<div class="technology-col">
			   <img src=" <?php echo get_theme_mod("icon6_setting");?>">
			</div>
          	<div class="technology-col">
			   <img src=" <?php echo get_theme_mod("icon7_setting");?>">
			</div>
          	<div class="technology-col">
			   <img src=" <?php echo get_theme_mod("icon8_setting");?>">
			</div>
             </div><!--technology-->
             <div class="clearfix"></div>
        </div>
    </div>
    
     
       <div class="section business-section">
      <div class="container innovative">
          <div class="row">
         	<div class="col-md-3 col-sm-6 col-xs-12 wow fadeInLeft"><div class="image">
				 <img src="<?php bloginfo('template_url');?>/assets/images/bag.svg" alt=""> 
				</div></div>
         	<div class="col-md-9 col-sm-6 col-xs-12 wow fadeInRight">
              <h1 class="title text-center sec-col-title"><?php echo get_theme_mod("business_oriented_title_setting");?></h1>
       <div class="innovative">
    	<p><?php echo get_theme_mod("business_oriented_description_setting");?></p>
         	</div> <!--innovative--> 
         	 </div>
         	
         </div>
       </div>
    </div>
    
    
  </section>
  <!--content-->
  <?php get_footer();?>