<?php 
/**
Template Name: Contact
**/

get_header();?>
<div class="container">
<div id="content">
		<div class="left-content">
			<?php
 			while ( have_posts() ): the_post();
			the_content();
			endwhile;
 			?>
		</div>
		<div class="right-content">
			<?php get_sidebar('contact');?>
		</div>
	</div>
</div>
  <!--content-->
  <?php get_footer();?>