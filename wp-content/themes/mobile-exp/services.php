   <?php /* Template Name: services */ get_header();?>
   <div id="banner" class="height">
    <div class="banner-in">
      <div class="banner-info">
        <div class="container">
          <div class="banner-content text-left wow fadeInUp" data-wow-duration="1s">
            <h2 class="banner-title"><?php echo get_theme_mod("custom_service_sbanner_title_setting");?></h2>
            <p><?php echo get_theme_mod("custom_service_banner_description_setting");?></p>
            <a href="contactus" class="custom-btn  services-btn">Let’s Talk</a> </div>
          <div class="image service-img">
		   <img class="wow fadeInUp"  data-wow-duration="2s" src="<?php echo get_theme_mod("custom_service_imageControl_setting");?>">
		  </div>
        </div>
      </div>
    </div>
  </div>
  <section id="content">
    <div id="what-we" class="section gray-bg">
      <div class="container-fluid">
        <div class="content-head text-center wow fadeInUp" data-wow-duration="1s">
          <h1 class="title"><?php echo get_theme_mod("custom_service_full_section_title_setting");?></h1>
          <p class="sub-title line"><?php echo get_theme_mod("custom_service_full_sec_sub_title_setting");?></p>
        </div>
        <!--content-head-->
        
        <ul id="filters" class="clearfix">
          <li><span class="filter" data-filter=".mobile-cat">Mobile</span></li>
          <li><span class="filter" data-filter=".webservices-cat">Web Services</span></li>
        </ul>
        <div class="row" id="portfoliolist">
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio mobile-cat" data-cat="mobile-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/ios-icon.png" alt=""></span>
              <h2 class="title2 spacing">iOS </h2>
              <p class="para-color m-0">We have knowledgeable iOS Developers. We are highly skilled in high-end development technologies such as Swift II utilizing Xcode 7 </p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio mobile-cat" data-cat="mobile-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/android-icon.png" alt=""></span>
              <h2 class="title2 spacing">ANDROID</h2>
              <p class="para-color m-0">Android is the world’s largest mobile platform and continues to grow by the day. Technology used by us includes the Android SDK.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio mobile-cat" data-cat="mobile-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/windows.png" alt=""></span>
              <h2 class="title2 spacing">WINDOWS</h2>
              <p class="para-color m-0">With the introduction of Microsoft’s newest operating system comes support for touch enabled applications. We use industry standard dev tools.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio mobile-cat" data-cat="mobile-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/cross-platform-icon.png" alt=""></span>
              <h2 class="title2 spacing">CROSS PLATFORM</h2>
              <p class="para-color m-0">Code once using HTML5, CSS and JavaScript, develop mobile apps for both iOS & Android.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio mobile-cat" data-cat="mobile-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/html5-icon.png" alt=""></span>
              <h2 class="title2 spacing">HTML 5.1</h2>
              <p class="para-color m-0">HTML 5 provides the power to develop visually stimulating and optimized mobile applications for every device in the Smartphone´s universe.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio webservices-cat" data-cat="webservices-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/e-commerce-icon.png" alt=""></span>
              <h2 class="title2 spacing">E-COMMERCE</h2>
              <p class="para-color m-0">We design and develop professional level e-commerce websites. </p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio webservices-cat" data-cat="webservices-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/info-icon.png" alt=""></span>
              <h2 class="title2 spacing">INFORMATIONAL </h2>
              <p class="para-color m-0">We build responsive and 
                user-friendly admin area CMS. Whether it’s WordPress or an Enterprise website we create amazing websites for you.</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-12 col-xs-12 portfolio webservices-cat" data-cat="webservices-cat">
            <div class="box text-center"> <span class="icon"><img src="<?php bloginfo('template_url');?>/assets/images/seo-marketing-icon.png" alt=""></span>
              <h2 class="title2 spacing">SEO / MARKETING</h2>
              <p class="para-color m-0">Search engine ranking is vital to a growing business. Let our team of search engine optimization and pay- per-click marketing specialists help take your company to the next level.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--what-we-->
    
        <div id="our-work" class="dark-gray-bg section">
      <div class="our-work-cnt text-center wow fadeInLeft" data-wow-duration="1s">
        <h1 class="title"><?php echo get_theme_mod("custom_our_work_title_setting");?></h1>
        <h2 class="title2"><?php echo get_theme_mod("custom_our_work_subtitle_setting");?></h2>
        <a href="project" class="custom-btn">See Our Work</a> </div>
      <!--our-work-cnt-->
      <div class="mobile wow fadeInUp" data-wow-duration="1s">
	  <img src=" <?php echo get_theme_mod("custom_our_work_imageControl_setting");?>"></div>
    </div>
    <!--what-we--> 
    
  </section>
  <!--content-->
  <!--content-->
  <?php get_footer();?>