<?php 
/*
@package sunset

			==================
               Mobile Express
			==================
*/


	/*
* Creating a function to create our CPT
*/
 
function testimonial() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'testimonials', 'Post Type General Name', 'MobiExpress' ),
        'singular_name'       => _x( 'testimonial', 'Post Type Singular Name', 'MobiExpress' ),
        'menu_name'           => __( 'Testimonials', 'MobiExpress' ),
        'parent_item_colon'   => __( 'Parent Movie', 'MobiExpress' ),
        'all_items'           => __( 'All Testimonials', 'MobiExpress' ),
        'view_item'           => __( 'View testimonial', 'MobiExpress' ),
        'add_new_item'        => __( 'Add New testimonial', 'MobiExpress' ),
        'add_new'             => __( 'Add New', 'MobiExpress' ),
        'edit_item'           => __( 'Edit testimonial', 'MobiExpress' ),
        'update_item'         => __( 'Update testimonial', 'MobiExpress' ),
        'search_items'        => __( 'Search testimonial', 'MobiExpress' ),
        'not_found'           => __( 'Not Found', 'MobiExpress' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'MobiExpress' ),
    );
     
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'testimonials', 'MobiExpress' ),
        'description'         => __( 'testimonial news and reviews', 'MobiExpress' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
     
    // Registering your Custom Post Type
    register_post_type( 'testimonials', $args );
 
}
 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
 
add_action( 'init', 'testimonial', 0 );
	


function mobile_express_widgets_init() {
register_sidebar( array(
	  'name'          => __( 'Widget Area', '' ),
	  'id'            =>    'sidebar-one',
	  'description'   => __( 'Add widgets here to appear in your sidebar.', '' ),
	  'before_widget' => '<section id="%1$s" class="widget wow fadeInUp %2$s">',
	  'after_widget'  => '</section>',
	  'before_title'  => '<h2 class="widget-title">',
	  'after_title'   => '</h2>',
   ) );
	
}
function create_post_type() {
	register_post_type( 'acme_product',
	  array(
		'labels' => array(
		  'name' => __( 'Products' ),
		  'singular_name' => __( 'Product' )
		),
		'public' => true,
		'has_archive' => true,
	  )
	);
  }
  
  function awesome_widget_setup(){
	  client_Testimonial_sidebar(
	  array(
	  'name' => 'sidebar',
	  'id'   =>  'sidebar-1',
	  'class' => 'custom',
	  'description' => 'standard sidebar',
	  
	  )
	  );
  }
  
  
// Navigation Menus
function my_dashboard_menu(){
	add_dashboard_page('My Dashboard Option','My Option','read','my-dashboard-option','my_menu_dash');
}

add_action('admin_menu','my_dashboard_menu');
//attach with action hook
// function themename_custom_logo_setup() {
// 	$defaults = array(
// 		'height' => 50,
// 		'width' => 177,
// 		'flex-height' => true,
// 		'flex-width' => true,
// 		'header-text' => array('site-title','site-description'),
// 	);
// 	add_theme_support('custom-logo', $defaults);
// }
function themename_custom_img_setup() {
	$defaults = array(
		'height' => 50,
		'width' => 177,
		'flex-height' => true,
		'flex-width' => true,
		
	);
	add_theme_support('custom-image', $defaults);
}
add_action('after_setup_theme','themename_custom_img_setup'); 

  add_action( 'init', 'create_post_type' );
 // Override theme default specification for product # per row
function loop_columns() {
return 5; // 5 products per row
}
add_filter('loop_shop_columns', 'loop_columns', 999);

add_action( 'widgets_init', 'mobile_express_widgets_init');





// Custom theme Customize option
function custom_theme_panel_settings($wp_customize){
	 // we add our custom Control
	 
	 $wp_customize->add_section("custom_theme_section_area",array(
	    "title"=>"Mobile Express Home Page Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_banner_title_setting",array(
	     "default"=>"CUSTOM PROCESS"
	 ));
	  $wp_customize->add_control("custom_banner_title_control",array(
	     "label"=>"Enter Banner Title",
		 "section"=>"custom_theme_section_area",
		  "settings"=>"custom_banner_title_setting"
	  ));
	  
	   //Text Area
	  $wp_customize->add_setting("custom_theme_banner_description_setting",array(
	     "default"=>"This is my Dummy Description Now...."
	 ));
	  $wp_customize->add_control("custom_theme_banner_description_control",array(
	     "label"=>"Put Your Banner Description",
		 "section"=>"custom_theme_section_area",
		  "settings"=>"custom_theme_banner_description_setting"
	  ));
	  
	 
	 //Banner ImageControl
	$wp_customize->add_setting("custom_banner_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_banner_imageControl",
	 array(
	    "label"=>"Upload Banner Image",
		"settings"=>"custom_banner_imageControl_setting",
		"section"=>"custom_theme_section_area"
	 )
	));
	 
	 
	 
//Banner End
	 
	  //section Custom Process
	  $wp_customize->add_setting("custom_theme_title_setting",array(
	     "default"=>"CUSTOM PROCESS"
	 ));
	  $wp_customize->add_control("custom_theme_title_control",array(
	     "label"=>"Home Page Mobile-develop section Title",
		 "section"=>"custom_theme_section_area",
		  "settings"=>"custom_theme_title_setting"
	  ));
	  
	   //Banner TextArea
	  $wp_customize->add_setting("custom_theme_description_setting",array(
	     "default"=>"This is my Dummy Description Now...."
	 ));
	  $wp_customize->add_control("custom_theme_description_control",array(
	     "label"=>"Home Page Mobile-develop section Description",
		 "section"=>"custom_theme_section_area",
		  "settings"=>"custom_theme_description_setting"
	  ));
	  
//Custom ImageControl
	$wp_customize->add_setting("custom_theme_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_theme_imageControl",
	 array(
	    "label"=>"Upload Banner Image",
		"settings"=>"custom_theme_imageControl_setting",
		"section"=>"custom_theme_section_area"
	 )
	));
//Custom Banner ImageControl
	$wp_customize->add_setting("custom_theme_Banner_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_theme_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_theme_Banner_imageControl_setting",
		"section"=>"custom_theme_section_area"
	 )
	));
	  
	  
	 //section Our Work
	 
	  $wp_customize->add_setting("custom_our_work_title_setting",array(
	     "default"=>"OUR WORK"
	 ));
	  $wp_customize->add_control("custom_our_work_title_control",array(
	     "label"=>"Home Page work section Title",
		 "section"=>"custom_theme_section_area",
		  "settings"=>"custom_our_work_title_setting"
	  ));
	  
	   //OurWork Sub_title
	  $wp_customize->add_setting("custom_our_work_subtitle_setting",array(
	     "default"=>"Let the work we’ve done speak for us"
	 ));
	  $wp_customize->add_control("custom_our_work_sub_title_control",array(
	     "label"=>"Home Page work section Sub Title",
		 "section"=>"custom_theme_section_area",
		  "settings"=>"custom_our_work_subtitle_setting"
	  ));
	  
//Custom ImageControl
	$wp_customize->add_setting("custom_our_work_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_our_work_imageControl",
	 array(
	    "label"=>"Home Page work section Side Image",
		"settings"=>"custom_our_work_imageControl_setting",
		"section"=>"custom_theme_section_area"
	 )
	));
  
  
//For AboutUs Page
	  
 $wp_customize->add_section("custom_theme_about_area",array(
	    "title"=>"About Us Page Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_aboutbanner_title_setting",array(
	     "default"=>"ABOUT US"
	 ));
	  $wp_customize->add_control("custom_aboutbanner_title_control",array(
	      "label"=>"About Section Title",
          "section"=>"custom_theme_about_area",
		  "settings"=>"custom_aboutbanner_title_setting"
	  ));

    $wp_customize->add_setting("custom_about_banner_description_setting",array(
	     "default"=>"This is my Dummy Description Now...."
	 ));
	  $wp_customize->add_control("custom_about_banner_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"custom_about_banner_description_setting"
	  ));	
      //about ImageControl
	$wp_customize->add_setting("custom_about_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_about_banner_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_about_imageControl_setting",
		"section"=>"custom_theme_about_area"
	 )
	));	 

//Aboutus mobile Express section
	  
// $wp_customize->add_section("about_mobile_express",array(
//	    "title"=>"About mobile app express Settings"
//	 ));
	  //Title Area
	  $wp_customize->add_setting("mobile_express_title_setting",array(
	     "default"=>"MOBILE APP EXPRESS"
	 ));
	  $wp_customize->add_control("mobile_express_title_control",array(
	     "label"=>"Mobile App Express Section Title",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"mobile_express_title_setting"
	  ));

    $wp_customize->add_setting("mobile_express_subtitle_setting",array(
	     "default"=>"Why Mobile App Express?"
	 ));
	  $wp_customize->add_control("mobile_express_subtitle_control",array(
	     "label"=>"SubTitle",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"mobile_express_subtitle_setting"
	  ));	
     $wp_customize->add_setting("mobile_express_description_setting",array(
	     "default"=>"Mobile App Express is a passionate, creative, and innovative mobile and web application company. There is no digital territory beyond our reach. We create revolutionary apps for individuals, start ups, and enterprises across every platform. More importantly we are a company that cares about seeing our partners succeed in their business. Every client and future business partner is special and unique; we make sure that every experience is treated as such."
	 ));
	  $wp_customize->add_control("mobile_express_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"mobile_express_description_setting"
	  ));


	//Aboutus Innovative section
	  
 $wp_customize->add_section("about_innovative",array(
	    "title"=>"About Innovative Section Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("innovative_title_setting",array(
	     "default"=>"INNOVATIVE"
	 ));
	  $wp_customize->add_control("innovative_title_control",array(
	     "label"=>"Innovative Section Title",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"innovative_title_setting"
	  ));

     $wp_customize->add_setting("innovative_description_setting",array(
	     "default"=>"If you can think it, we can build it. If there is a problem, we find a solution. You won’t find any pessimist in our ranks. Innovation is the product of imagination. We know your dream is possible and dreams don’t come with instructions."
	 ));
	  $wp_customize->add_control("innovative_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"innovative_description_setting"
	  ));
   //innovative ImageControl
	$wp_customize->add_setting("innovative_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_banner_innovative_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"innovative_imageControl_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
//Aboutus Experienced section
	  
$wp_customize->add_section("about_experinced",array(
	    "title"=>"About Experienced Section Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("experinced_title_setting",array(
	     "default"=>"EXPERIENCED"
	 ));
	  $wp_customize->add_control("experinced_title_control",array(
	     "label"=>"Experinced Section Title",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"experinced_title_setting"
	  ));

     $wp_customize->add_setting("experinced_description_setting",array(
	     "default"=>"If you can think it, we can build it. If there is a problem, we find a solution. You won’t find any pessimist in our ranks. Innovation is the product of imagination. We know your dream is possible and dreams don’t come with instructions."
	 ));
	  $wp_customize->add_control("experinced_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"experinced_description_setting"
	  ));
    //Experinced image
	$wp_customize->add_setting("experinced_image_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_Experinced SideImage",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"experinced_image_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
   //Experinced image
	$wp_customize->add_setting("experinced_banner_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_Experinced BannerImage",
	 array(
	    "label"=>"Upload Banner Image",
		"settings"=>"experinced_banner_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
    
   //icons image
	$wp_customize->add_setting("icon1_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon1Control",
	 array(
	    "label"=>"Upload Icon1",
		"settings"=>"icon1_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	//icon2 image
	
	$wp_customize->add_setting("icon2_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon2Control",
	 array(
	    "label"=>"Upload Icon2",
		"settings"=>"icon2_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	//icon3 image
	
	$wp_customize->add_setting("icon3_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon3Control",
	 array(
	    "label"=>"Upload Icon3",
		"settings"=>"icon3_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	//icon4 image
	
	$wp_customize->add_setting("icon4_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon4Control",
	 array(
	    "label"=>"Upload Icon4",
		"settings"=>"icon4_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	//icon5 image
	
	$wp_customize->add_setting("icon5_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon5Control",
	 array(
	    "label"=>"Upload Icon5",
		"settings"=>"icon5_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	//icon6 image
	
	$wp_customize->add_setting("icon6_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon6Control",
	 array(
	    "label"=>"Upload Icon6",
		"settings"=>"icon6_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	//icon7 image
	
	$wp_customize->add_setting("icon7_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon7Control",
	 array(
	    "label"=>"Upload Icon7",
		"settings"=>"icon7_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	//icon8 image
	
	$wp_customize->add_setting("icon8_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_icon8Control",
	 array(
	    "label"=>"Upload Icon8",
		"settings"=>"icon8_setting",
		"section"=>"custom_theme_about_area"
	 )
	));
	
	
	//Aboutus Business Oriented section
	  
   $wp_customize->add_section("about_business_oriented",array(
	    "title"=>"About Business Oriented Section Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("business_oriented_title_setting",array(
	     "default"=>"INNOVATIVE"
	 ));
	  $wp_customize->add_control("business_oriented_title_control",array(
	     "label"=>"Business Oriented Section Title",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"business_oriented_title_setting"
	  ));

     $wp_customize->add_setting("business_oriented_description_setting",array(
	     "default"=>"If you can think it, we can build it. If there is a problem, we find a solution. You won’t find any pessimist in our ranks. Innovation is the product of imagination. We know your dream is possible and dreams don’t come with instructions."
	 ));
	  $wp_customize->add_control("business_oriented_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_about_area",
		  "settings"=>"business_oriented_description_setting"
	  ));

	
	// for Services page
		
	//Banner Section
	  
    $wp_customize->add_section("custom_theme_service_Banner_area",array(
	    "title"=>"Services Page Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_service_sbanner_title_setting",array(
	     "default"=>"SERVICES"
	 ));
	  $wp_customize->add_control("custom_service_banner_title_control",array(
	     "label"=>"Services Section Title",
		 "section"=>"custom_theme_service_Banner_area",
		  "settings"=>"custom_service_sbanner_title_setting"
	  ));

    $wp_customize->add_setting("custom_service_banner_description_setting",array(
	     "default"=>"This is my Dummy Description Now...."
	 ));
	  $wp_customize->add_control("custom_service_banner_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_service_Banner_area",
		  "settings"=>"custom_service_banner_description_setting"
	  ));	
 //projects ImageControl
	$wp_customize->add_setting("custom_service_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_service_banner_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_service_imageControl_setting",
		"section"=>"custom_theme_service_Banner_area"
	 )
	));	
	
	//For second Section
	
//	   $wp_customize->add_section("custom_service_full_cycle_Banner_area",array(
//	    "title"=>"Services full cycle Section Settings"
//	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_service_full_section_title_setting",array(
	     "default"=>"FULL CYCLE DEVELOPMENT"
	 ));
	  $wp_customize->add_control("custom_service_full_section_title_control",array(
	     "label"=>"Enter Banner Title",
		 "section"=>"custom_theme_service_Banner_area",
		  "settings"=>"custom_service_full_section_title_setting"
	  ));

    $wp_customize->add_setting("custom_service_full_sec_sub_title_setting",array(
	     "default"=>"This is my Dummy Description Now...."
	 ));
	  $wp_customize->add_control("custom_service_full_sec_sub_title_control",array(
	     "label"=>"Put Your Description",
		 "section"=>"custom_theme_service_Banner_area",
		  "settings"=>"custom_service_full_sec_sub_title_setting"
	  ));	
	  
	  
	  
		  
	//For Project Page
	
	//Banner Section
	  
    $wp_customize->add_section("custom_theme_projects_area",array(
	    "title"=>"Project Page Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_projectsbanner_title_setting",array(
	     "default"=>"SERVICES"
	 ));
	  $wp_customize->add_control("custom_projectsbanner_title_control",array(
	     "label"=>"Project Section Title",
		 "section"=>"custom_theme_projects_area",
		  "settings"=>"custom_projectsbanner_title_setting"
	  ));

    $wp_customize->add_setting("custom_projects_banner_description_setting",array(
	     "default"=>"This is my Dummy Description Now...."
	 ));
	  $wp_customize->add_control("custom_projects_banner_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_projects_area",
		  "settings"=>"custom_projects_banner_description_setting"
	  ));	
      //projects ImageControl
	$wp_customize->add_setting("custom_projects_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_project_banner_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_projects_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	
	//GameMasters Development

	  //Title Area
	  $wp_customize->add_setting("custom_projects_game_masters_title_setting",array(
	     "default"=>"FULL CYCLE DEVELOPMENT"
	 ));
	  $wp_customize->add_control("custom_projects_game_masters_title_control",array(
	     "label"=>"Game master Title",
		 "section"=>"custom_theme_projects_area",
		  "settings"=>"custom_projects_game_masters_title_setting"
	  ));

    $wp_customize->add_setting("custom_projects_game_masters_sub_title_setting",array(
	     "default"=>"Mobile App Express offers a full range of software and service solutions."
	 ));
	  $wp_customize->add_control("custom_projects_game_masters_sub_title_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_projects_area",
		 "settings"=>"custom_projects_game_masters_sub_title_setting"
	  ));
	  
	//gamemasters ImageControl
	
	$wp_customize->add_setting("custom_game_masters_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_gm_right_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_game_masters_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	//game masters Icon
		
	$wp_customize->add_setting("custom_game_masters_icon_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_gm_icon_imageControl",
	 array(
	    "label"=>"icon1",
		"settings"=>"custom_game_masters_icon_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	$wp_customize->add_setting("custom_game_masters_icon2_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_gm_icon2_imageControl",
	 array(
	    "label"=>"icon2",
		"settings"=>"custom_game_masters_icon2_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	
	//F Development

	  
	  //Title Area
	  $wp_customize->add_setting("custom_projects_F_title_setting",array(
	     "default"=>"FULL CYCLE DEVELOPMENT"
	 ));
	  $wp_customize->add_control("custom_projects_F_title_control",array(
	     "label"=>"Socially Fit Title",
		 "section"=>"custom_theme_projects_area",
		  "settings"=>"custom_projects_F_title_setting"
	  ));

    $wp_customize->add_setting("custom_projects_F_desc_setting",array(
	     "default"=>"Mobile App Express offers a full range of software and service solutions."
	 ));
	  $wp_customize->add_control("custom_projects_F_desc_control",array(
	     "label"=>"Socially Fit Description",
		 "section"=>"custom_theme_projects_area",
		 "settings"=>"custom_projects_F_desc_setting"
	  ));
	  
	//F ImageControl
	
	$wp_customize->add_setting("custom_f_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_f_left_imageControl",
	 array(
	    "label"=>"Upload Socially Fit Image",
		"settings"=>"custom_f_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	

	//icons
	
	$wp_customize->add_setting("custom_f_icon_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_F_icons_imageControl",
	 array(
	    "label"=>"Upload logo first",
		"settings"=>"custom_f_icon_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	$wp_customize->add_setting("custom_f_icon2_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_f_icon2_imageControl",
	 array(
	    "label"=>"Upload logo Second",
		"settings"=>"custom_f_icon2_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	
	
	
	//For Process Page
	
	//Banner Section
	  
    $wp_customize->add_section("custom_theme_processs_area",array(
	    "title"=>"Process page Settings"
	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_processsbanner_title_setting",array(
	      "default"=>"PROCESS",
        ));
	  $wp_customize->add_control("custom_processsbanner_title_control",array(
         "label"=>"Process Section Title",
		 "section"=>"custom_theme_processs_area",
		  "settings"=>"custom_processsbanner_title_setting"
	  ));

    $wp_customize->add_setting("custom_processs_banner_description_setting",array(
	     "default"=>"Each process is customized to the client, but below is an example of our three-step development process."
	 ));
	  $wp_customize->add_control("custom_processs_banner_description_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_processs_area",
		  "settings"=>"custom_processs_banner_description_setting"
	  ));
	  
      //process Image Control
	$wp_customize->add_setting("custom_process_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_banner_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_process_imageControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	
	
	//step1


	  //step1 Title Area
	  $wp_customize->add_setting("custom_process_step1_title_setting",array(
	     "default"=>"FULL CYCLE DEVELOPMENT"
	 ));
	  $wp_customize->add_control("custom_process_step1_title_control",array(
	     "label"=>"Step 1 Title",
		 "section"=>"custom_theme_processs_area",
		  "settings"=>"custom_process_step1_title_setting"
	  ));

	  $wp_customize->add_setting("custom_process_step1_desc_setting",array(
	     "default"=>"Mobile App Express offers a full range of software and service solutions."
	 ));
	  $wp_customize->add_control("custom_process_step1_desc_control",array(
	     "label"=>" Description",
		 "section"=>"custom_theme_processs_area",
		 "settings"=>"custom_process_step1_desc_setting"
	  ));
	  
	  
	//step1 ImageControl
	
	$wp_customize->add_setting("process_step1_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_step1_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"process_step1_imageControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	  
		//step1 IconControl
	
	$wp_customize->add_setting("process_step1_iconControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_step1_iconControl",
	 array(
	    "label"=>"Upload Step 1 Icon",
		"settings"=>"process_step1_iconControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	
	
		//step2 IconControl
	
	$wp_customize->add_setting("process_step2_iconControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_step2_iconControl",
	 array(
	    "label"=>"Upload Step 2 Icon",
		"settings"=>"process_step2_iconControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	
	//step3 IconControl
	
	$wp_customize->add_setting("process_step3_iconControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_step3_iconControl",
	 array(
	    "label"=>"Upload Step 3 Icon",
		"settings"=>"process_step3_iconControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	
	
	//step2 Title Area  
		  
	  $wp_customize->add_setting("custom_process_step2_title_setting",array(
	     "default"=>"Process page step2 customization"
	 ));
	  $wp_customize->add_control("custom_process_step2_title_control",array(
	     "label"=>"Step 2 Title",
		 "section"=>"custom_theme_processs_area",
		  "settings"=>"custom_process_step2_title_setting"
	  ));

	  
	  $wp_customize->add_setting("custom_process_step2_desc_setting",array(
	     "default"=>"Mobile App Express offers a full range of software and service solutions."
	 ));
	  $wp_customize->add_control("custom_process_step2_desc_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_processs_area",
		 "settings"=>"custom_process_step2_desc_setting"
	  ));
	  
	  	//step2 ImageControl
	
	$wp_customize->add_setting("process_step2_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_step2_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"process_step2_imageControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	  
	 //step3 Title Area
	 
	$wp_customize->add_section("custom_theme_process_step3_area",array(
	    "title"=>"Process page step3 customization"
	 ));
	  $wp_customize->add_setting("custom_process_step3_title_setting",array(
	     "default"=>"Process page step3 customization"
	 ));
	  $wp_customize->add_control("custom_process_step3_title_control",array(
	     "label"=>"Step 3 Title",
		 "section"=>"custom_theme_processs_area",
		  "settings"=>"custom_process_step3_title_setting"
	  ));

	  $wp_customize->add_setting("custom_process_step3_desc_setting",array(
	     "default"=>"Mobile App Express offers a full range of software and service solutions."
	 ));
	  $wp_customize->add_control("custom_process_step3_desc_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_processs_area",
		 "settings"=>"custom_process_step3_desc_setting"
	  ));
	  
	//step3 ImageControl
	
	$wp_customize->add_setting("process_step3_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_process_step3_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"process_step3_imageControl_setting",
		"section"=>"custom_theme_processs_area"
	 )
	));	
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	//gamemasters ImageControl
	
	$wp_customize->add_setting("custom_game_masters_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_gm_right_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_game_masters_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	//game masters Icon
		
	$wp_customize->add_setting("custom_game_masters_icon_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_gm_icon_imageControl",
	 array(
	    "label"=>" Icon1",
		"settings"=>"custom_game_masters_icon_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	$wp_customize->add_setting("custom_game_masters_icon2_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_gm_icon2_imageControl",
	 array(
	    "label"=>"Icon2",
		"settings"=>"custom_game_masters_icon2_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	
	//F Development

	  //Title Area
	  $wp_customize->add_setting("custom_projects_F_title_setting",array(
	     "default"=>"FULL CYCLE DEVELOPMENT"
	 ));
	  $wp_customize->add_control("custom_projects_F_title_control",array(
	     "label"=>"Socially Fit Section Title",
		 "section"=>"custom_theme_projects_area",
		  "settings"=>"custom_projects_F_title_setting"
	  ));

    $wp_customize->add_setting("custom_projects_F_desc_setting",array(
	     "default"=>"Mobile App Express offers a full range of software and service solutions."
	 ));
	  $wp_customize->add_control("custom_projects_F_desc_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_projects_area",
		 "settings"=>"custom_projects_F_desc_setting"
	  ));
	  
	//F ImageControl
	
	$wp_customize->add_setting("custom_f_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_f_left_imageControl",
	 array(
	    "label"=>"Upload Image",
		"settings"=>"custom_f_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	

	//icons
	
	$wp_customize->add_setting("custom_f_icon_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_F_icons_imageControl",
	 array(
	    "label"=>"Icon1",
		"settings"=>"custom_f_icon_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	$wp_customize->add_setting("custom_f_icon2_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_f_icon2_imageControl",
	 array(
	    "label"=>"Icon2",
		"settings"=>"custom_f_icon2_imageControl_setting",
		"section"=>"custom_theme_projects_area"
	 )
	));	
	
	
	//For ContactUs
	 $wp_customize->add_section("custom_theme_contactus_page",array(
		"title"=>"Contact US page Setting"
	 ));
	  //Title Area
	  $wp_customize->add_setting("custom_contactus_title_setting",array(
	     "default"=>"CONTACT US"
	 ));
	  $wp_customize->add_control("custom_contactus_F_title_control",array(
	     "label"=>"Contact Us Title",
		 "section"=>"custom_theme_contactus_page",
		  "settings"=>"custom_contactus_title_setting"
	  ));

    $wp_customize->add_setting("custom_contactus_subtitle_setting",array(
	     "default"=>"Ready to get your project started? Ready to get your project started?
           We are here to help."
	 ));
	  $wp_customize->add_control("custom_contactus_subtitle_control",array(
	     "label"=>"Description",
		 "section"=>"custom_theme_contactus_page",
		 "settings"=>"custom_contactus_subtitle_setting"
	  ));
     //Banner ImageControl
	
	$wp_customize->add_setting("custom_contactus_banner_imageControl_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_contact-us_banner_imageControl",
	 array(
	    "label"=>"Upload banner Image",
		"settings"=>"custom_contactus_banner_imageControl_setting",
		"section"=>"custom_theme_contactus_page"
	 )
	));	
	
	//more contact info

	  $wp_customize->add_setting("custom_contactus_email_setting",array(
	     "default"=>"Contact@MobileAppExpress.com"
	 ));
	  $wp_customize->add_control("custom_contactus_email_control",array(
	     "label"=>"Enter First Mail",
		 "section"=>"custom_theme_contactus_page",
		  "settings"=>"custom_contactus_email_setting"
	  ));

	   $wp_customize->add_setting("custom_contactus_email2_setting",array(
	     "default"=>"Contact@MobileAppExpress.com"
	 ));
	  $wp_customize->add_control("custom_contactus_email2_control",array(
	     "label"=>"Enter Second Mail",
		 "section"=>"custom_theme_contactus_page",
		  "settings"=>"custom_contactus_email2_setting"
	  ));
	  
	   $wp_customize->add_setting("custom_contactus_phonenum_setting",array(
	     "default"=>"800)-610-4054"
	 ));
	  $wp_customize->add_control("custom_contactus_phonenum_control",array(
	     "label"=>"Enter First Number",
		 "section"=>"custom_theme_contactus_page",
		  "settings"=>"custom_contactus_phonenum_setting"
	  ));
	  
	   $wp_customize->add_setting("custom_contactus_phonenum2_setting",array(
	     "default"=>"(800)-610-4054"
	 ));
	  $wp_customize->add_control("custom_contactus_phonenum2_control",array(
	     "label"=>"Enter Second Number",
		 "section"=>"custom_theme_contactus_page",
		  "settings"=>"custom_contactus_phonenum2_setting"
	  ));
	  
    $wp_customize->add_setting("custom_contactus_privacy_setting",array(
	     "default"=>"Mobile App Express respects your privacy. We will not share your information with any 3rd parties without your permission. We take strict security measures and we will sign an Non-Disclosure Agreements to ensure the confidentiality of all your intellectual property, code and data."
	 ));
	  $wp_customize->add_control("custom_contactus_privacy_setting_control",array(
	     "label"=>"Put Your Privacy Policy",
		 "section"=>"custom_theme_contactus_page",
		 "settings"=>"custom_contactus_privacy_setting"
	  ));
    
     //step3 Contact us more info Section
    
	$wp_customize->add_setting("custom_contactus_moreinfo_img_setting");
	
	$wp_customize->add_control(new WP_Customize_Image_Control(
	$wp_customize,
	 "custom_contactus_banner_imageControl",
	 array(
	    "label"=>"Contact info Banner",
		"settings"=>"custom_contactus_moreinfo_img_setting",
		"section"=>"custom_theme_contactus_page"
	 )
	));	
    
}



// textArea

add_action ("customize_register","custom_theme_panel_settings")
	
 

?>














