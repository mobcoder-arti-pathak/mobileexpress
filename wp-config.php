<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mobile_express');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '!!egend@ry_web');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<d<@fdSLUSpggoL<aIRfOSB:I[}-HT1(MDY#>U1[5)GL;+mr3ftc4(^gPw*X{r,W');
define('SECURE_AUTH_KEY',  'wG+0<]IcKyTnyWX@Ly.[z0WDQrIztxS@qrubm0|tqjJblvmwi~iF;/jK{N52<o)6');
define('LOGGED_IN_KEY',    'OT!L8Mtq78@0`Sx;>c7v!:6]*F}^F#>R(m)Mx+TOIt6<F`=KytxC+>La(bFQ.z`|');
define('NONCE_KEY',        'K@6+*UHPl*c=T.%`,LA)+m!tBax$kd*ly(0*yQwJ=9IPz,QP9kKJrL*z0JmBHdl_');
define('AUTH_SALT',        'Y.yZl&6iSHklRcrs|WwYFh`syN^zSJ5J~oXYLF$uBO/s,@l0Cy~%`|S>)0YU|QFb');
define('SECURE_AUTH_SALT', '|A=]8%C7aqvh3*[1l~8L4%r$6};,}|ULk-hmL8mzIfCLMTgV@KsWZVa43Ebh~pLq');
define('LOGGED_IN_SALT',   '@EMh%T`9&+Ln 6+~n+%uotoj.(o.[Nywv+wuBs>it5$H`>@GnS%Yf{K1f6w+YzNt');
define('NONCE_SALT',       'W3%x|MoZ5n&uqR7c?j*RpF1A|OXyB}oQNaJbsK.Xg2$fx,>/(OIYe(ym5g U<n}L');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('WP_MEMORY_LIMIT', '64M');
